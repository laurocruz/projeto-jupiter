// Curso de Git - CACo

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    int hidro, gas = 0, gravidade = 0, helio = 0;

    hidro = stoi(argv[1], nullptr);
    
    gas = hidro/50;
    hidro %= 50;

    gravidade = hidro/10;
    hidro %= 10;

    helio = hidro/5;
    hidro %= 5;


    /*
    // gas = 50 hidrogenio
    while (hidro - 50 >= 0) {
        gas++;
        hidro -= 50;
    }
    // gravidade = 10 hidrogenio
    while (hidro - 10 >= 0) {
        gravidade++;
        hidro -= 10;
    }
    // helio = 5 hidrogenio
    while (hidro - 5 >= 0) {
        helio++;
        hidro -= 5;
    }
    */

    cout << hidro << " hidrogênio" << endl;
    cout << helio << " hélio" << endl;
    cout << gravidade << " gravidade" << endl;
    cout << gas << " gás" << endl;

    return 0;
}
